Within the context of a local copy of a SlackBuilds.org repository,
*HooRex* answers the question of who requires a particular package i.e. which
packages name the package of interest in the REQUIRES field of their .info
file? A query like this about, say, package json-c could be answered by
something like:

```find /x/y/slackbuilds -name \*.info |xargs grep REQUIRES |grep json-c```

However you'll need to wait while all the .info files in the repository are
searched; that's OK for the occasional query but increasingly tiresome for
multiple queries. You'll also need to wade through all the output to pick out
the actual package names - also quite tiresome if you need to accurately supply
package names to some other script or program.

*HooRex* addresses these issues (and more) by generating and cacheing the
dependency relationships between all packages in the repository, enabling a
rapid response to dependency queries.

The packages listed by HooRex are always displayed in build order i.e. if any
packages depend on each other, the depending packages are displayed after the
packages they depend on.

**Workflow example.**  
    After a weekly SBo update, a list of packages which have been updated (hence
need rebuilding) and what other packages depend on them (and may also require
rebuilding) can be derived as follows. First, cut and paste the list of updates
from the update notification email and save them into a file named, say,
"clist". You could inspect the file and remove any entries you consider
unimportant e.g. a change to the copyright comment of the qt5.SlackBuild
probably doesn't warrant a rebuild of qt5 and all packages that depend on it.
The file's unwanted short comments can be left intact since we can remove them
on the fly when running *HooRex* e.g.
```
cut -d: -f1 clist | hoorex -l1
```
displays a build ordered list of all updated packages remaining in the clist
file, along with any packages depending on them. The updated packages are shown
in bold text; the dependent packages in plain text.

Of course, you're probably only interested in the changed packages amongst those
which you already have installed. In that case, don't bother with looping
through all the entries in /var/log/packages; just run:
```
cut -d: -f1 clist | hoorex -Il1
```
Again, updated packages are displayed in bold text; dependent packages in plain
text.

**Command line TAB completion.**  
  Command line Tab completion is now available for zsh (thanks to David
O'Shaughnessy) as well as for bash. Completions for *HooRex* should just work
for any new zsh instances invoked after *HooRex* has been installed. For
completions using bash, ensure that the bash-completions package (available from
the Slackware repo's "extra" section) has been installed.

**Quirk file support.**  
  *HooRex* is now capable of including additional dependency information
recorded in a quirks file. This facility enables extra packages to be declared
as required for a particular build as if it were declared in the REQUIRES field
of the .info file. A typical use case is a SlackBuild with optional support for
python3, where python3 support may be included if python3 is detected at build
time. How can the need for python3 to be installed be expressed without editing
the .info file and, therefore, being out of sync with the official SBo repo?
A separate quirks file addresses this problem; additional required packages can
be declared without interfering with any .info files. *HooRex* consults the
quirks file and includes any additional required packages from it whenever the
dependency tree is being regenerated (-f option). The default location of the
quirks file is at ~/.local/share/hoorex/quirks but a different location may be
set with the -q (--quirkFile) option e.g.
```
hoorex -q /var/cache/vmbuilder/quirks
```

The quirks file lists packages with additional requirements in .INI format.
Each section is named after a SlackBuild. The additional package requirements
follow in a field named QK_REQUIRES. Thus, to add the auto detected packages
libdc1394 and ffmpeg to the build of opencv, the quirks file entry would be:
```
[opencv]
QK_REQUIRES = "libdc1394 ffmpeg"
```
Whenever a new quirks file is is specified like this, the dependency tree is
automatically recalculated to take into account any quirks contained at the new
location.

As a side note, each quirk file section may have additional fields which are
not recognised by *HooRex* but which could be utilised by other software.
In particular, the QK_ENVOPTS field can specify values for environment variables
that may be needed for some SlackBuilds. For example, the SlackBuild for opencv
can include support for gdal. However, according to the README, as well as the
gdal package itself needing to be installed, the SlackBuild depends on the
environment variable CVGDAL=yes to be set. Dedicated building software could
extract any such needs from the quirks file and set the environment accordingly.
In this case, a complete quirks file entry for opencv would be:
```
[opencv]
QK_REQUIRES = "libdc1394 ffmpeg gdal"
QK_ENVOPTS = "CVGDAL=yes"
```
